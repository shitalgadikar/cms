**Protractor Cucumber framework**

This framework was part of 'angular/protractor' and now it is seperated to decouple cucucmber.js

**Steps to create protractor cucumber project:**
1. Create folder 
2. Open terminal and change directory path to project folder
3. To create package.json enter following command
    npm init 
4. Install protractor as a dev dependency in package.json file by using following command
    npm install --save-dev protractor
    
   Then update the webdriver-manager, which is helper tool to easily get instance of selenium server running
    webdriver-manager update
5. Install cucumber as a dev dependency in package.json file using following command:
    npm install --save-dev cucumber
    
6. Install protractor-cucumber-framework using following command
    npm install --save-dev protractor-cucumber-framework    
   
7. Configure Protractor Configuration file To Run With Cucumber- copy following code 
    exports.config = {
        chromeDriver: "/home/iauro-qa004/Downloads/chromedriver",
        seleniumAddress: 'http://localhost:4444/wd/hub',
        specs: ["../feature/*.feature"],
    
        baseUrl: 'http://www.way2automation.com/angularjs-protractor/registeration/#/login',
        capabilities:{
            browserName: "chrome",
            chromeOptions: {
                args: [
                    // disable chrome's infobar
                    '--disable-infobars',
                    '--no-sandbox' // Chrome won't be able to startup. then this option is used
                ]
            }
        },
        framework:'custom',
        frameworkPath: require.resolve("protractor-cucumber-framework"),
        output: '../output.json',
        cucumberOpts:{
            require: ['../stepDefinition/*.js'],
            // format:'pretty',
            tags: false
        }
    };

8. Create feature file : lets take an example of login
    Feature: Testing login scenario
      Scenario: Testing login
        Given I am on login page
        When I enter username
        And enter password
        And Click on Login
        Then I should see homepage
      
9. Create step definition file and copy following code
    // Scenario: Testing login
    const { browser } = require("protractor");
    const { Given, When, Then } = require("cucumber");
        // Given I am on login page
        Given('I am on login page', function () {
            browser.driver.manage().window().maximize(); // To maximize the window
            return browser.get("http://www.way2automation.com/angularjs-protractor/registeration/#/login");
        });
    
        // When I enter username
        When('I enter username', function () {
            element(by.id("username")).sendKeys("angular");
            return element(by.id("formly_1_input_username_0")).sendKeys("angular");
        });
    
        //And enter password
        When('enter password', function () {
           return element(by.id("password")).sendKeys("password");
        });
    
       // And Click on Login
        When('Click on Login', function () {
            return element(by.css("body > div.jumbotron > div > div > div > form > div.form-actions > button")).click();
        });
    
        // Then I should see homepage
        Then('I should see homepage', function () {
            console.log("On Home Page..!!");
        });
        
        
  Now we are done with all the setup, it's time to run our test.
  
  **Run The Test**
  
  To run the test use following command:
    npm test
         